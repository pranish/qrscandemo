# README #

### What is this repository for? ###

Quick summary
This is a sample app which scans QR code, decodes the information and shows the information. 
After scanning the QR code, the app seceretly takes picture without the user knowing it (i.e no camera preview is shown)

![Scheme] (app/src/main/res/drawable/ic_screenshot.png) 

### How do I get set up? ###
#### Summary of set up ####
The app uses Zxing Scanner (https://play.google.com/store/apps/details?id=com.google.zxing.client.android) for scanning QR codes.
If the device doesn't have Zxing Scanner then the app will redirect the user to playstore to download the app.

#### Dependencies ####
QR code scanner library - 'me.dm7.barcodescanner:zxing:1.8.4'

Hidden camera library - 'com.kevalpatel2106:hiddencamera:1.3.2'

### Who do I talk to? ###
#### Repo owner or admin ####
   Please drop a mail at pranish.shres@gmail.com for any queries.