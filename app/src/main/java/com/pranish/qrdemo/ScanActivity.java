package com.pranish.qrdemo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.google.zxing.Result;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

/**
 * Use this class if you don't want to install Zxing Scanner.
 * But using scanned from this code is comparatively slower than Zxing Scanner
 */
public class ScanActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {

    private ZXingScannerView scannerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        scannerView = new ZXingScannerView(this);
        setContentView(scannerView);
        scannerView.setResultHandler(this);
        scannerView.startCamera(1);
    }

    @Override
    public void handleResult(Result result) {
        Intent resultIntent = new Intent();
        resultIntent.putExtra("SCAN_RESULT", result.getText());
        setResult(RESULT_OK, resultIntent);
        onBackPressed();
        finish();
    }


    @Override
    public void onBackPressed() {
        scannerView.stopCamera();
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        if (scannerView != null)
            scannerView.stopCamera();
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        scannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        scannerView.startCamera();          // Start camera on resume
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

}
