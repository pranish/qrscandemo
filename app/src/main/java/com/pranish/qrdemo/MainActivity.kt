package com.pranish.qrdemo

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by pranishshrestha on 9/20/17.
 */
class MainActivity : AppCompatActivity() {

    private lateinit var btnScan: Button
    private lateinit var txtInfo: TextView
    private lateinit var imgQRCode: ImageView
    private lateinit var txtTitle: TextView
    private lateinit var contents: String

    private var REQUEST_CODE: Int = 101

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        findViews()
        init()
    }

    private fun findViews() {
        btnScan = findViewById(R.id.btnScan) as Button
        txtInfo = findViewById(R.id.txtInfo) as TextView
        imgQRCode = findViewById(R.id.imgBarCode) as ImageView
        txtTitle = findViewById(R.id.txtTitle) as TextView
    }

    private fun init() {
        btnScan.setOnClickListener {
             /** Use ScanActivity class if you don't want user to download Zxing Scanner**/
            var intent: Intent = Intent("com.google.zxing.client.android.SCAN");
            if (getPackageManager().queryIntentActivities(intent,
                    PackageManager.MATCH_DEFAULT_ONLY).size == 0) {
                val marketUri = Uri.parse("market://details?id=com.google.zxing.client.android")
                val marketIntent = Intent(Intent.ACTION_VIEW, marketUri)
                startActivity(marketIntent)
            } else {
                intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
                intent.putExtra("SCAN_CAMERA_ID", 1);  // TODO change the value to 1 for front camera
                startActivityForResult(intent, REQUEST_CODE)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode != Activity.RESULT_CANCELED) {
            if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK) {
                contents = data?.getStringExtra("SCAN_RESULT").toString() //this is the result
                txtInfo.text = contents
                txtTitle.text = "Scan Successful"
                startService(Intent(this, ImageService().javaClass))
            }
        }
    }

}